<?php
namespace SkeletonBundle;

use SkeletonBundle\DependencyInjection\SkeletonExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл с курсами
 *
 * Class SkeletonBundle
 *
 * @package SkeletonBundle
 */
class SkeletonBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new SkeletonExtension();
    }
}