<?php

namespace SkeletonBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 *
 * @package SkeletonBundle\Controller
 */
class DefaultController extends AbstractController
{
    #[Route(path: '/skeleton/', name: 'shop.manage.product.create')]
    public function update(Request $request): Response
    {
        return $this->render('@Skeleton/Default/index.html.twig', []);
    }
}
