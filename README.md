# Создание бандла Symfony 5

### Копируем SkeletonBundle
### Заменяем везде Skeleton на новое название бандла

### Добавляем бандл в проект
#### composer.json: [здесь](/composer.json)
    "autoload": {
        "psr-4": {
            "App\\": "src/",
            "SkeletonBundle\\": "bundles/SkeletonBundle/"
        }
    },

### Add Bundle to bundles.php [здесь](/config/bundles.php)
    SkeletonBundle\SkeletonBundle::class => ['all' => true],

### Regenerate autoload:
    composer dump-autoload
    docker-compose run --rm php-cli composer dump-autoload


### Заменяем все названия Skeleton

### DI и Конфигурация
### Добавляем конфигурационный файл skeleton.yaml [здесь](/config/packages)
    skeleton:
        mode: "test"


### Добавляем роуты
#### config/routes.yaml [здесь](/config/routes.yaml)
    new_bundle:
        resource: '@SkeletonBundle/Controller/'
        type:     annotation


### Twig [здесь](/config/packages/twig.yaml)
#### config/packages/twig.yaml
    twig:
        default_path: '%kernel.project_dir%/templates'
        debug: '%kernel.debug%'
        strict_variables: '%kernel.debug%'
        form_themes: ['bootstrap_4_layout.html.twig']
        ...
        paths:
            '%kernel.project_dir%/bundles/SkeletonBundle':

#### ide-twig.json  [здесь](/ide-twig.json)
    {
        "namespaces": [
            { "path": "templates" },
            { "path": "bundles/SkeletonBundle" }
        ]
    }

----

#### migrationsPath  [здесь](/config/packages/doctrine_migrations.yaml)
```config/packages/doctrine_migrations.yaml```

```'SocialBundle\DoctrineMigrations': '@TradingBundle/DoctrineMigrations'```

----

## Ньюансы

### Контроллеры
    /**
    * @Route("/file", name="file")
    *
    * @Template(template="/templates/Default/test.html.twig")
    * @return Response
    */
      public function test()
      {
          dump([213]);
          return [
            'vars' => '342'
          ];
      }


### Добавляем репозиторий удвленный/локальный

    "repositories": [
        {
            "type" : "path",
            "url" : "./bundles/SkeletonBundle"
        }
    ],
---
    "repositories": [
        {
            "type" : "vcs",
            "url" : "git@bitbucket.org:emilasp/SkeletonBundle.git"
        }
    ],


### Maker для бандла
#### config/packages/dev/maker.yaml
    maker:
        root_namespace: UserBundle


#### config/packages/doctrine.yaml  [здесь](/config/packages/doctrine.yaml)
doctrine:
    orm:
        mappings:
            UserBundle:
                is_bundle: true
                type: annotation
                #dir: '/Entity'
                prefix: SkeletonBundle\Entity
                alias: SkeletonBundle

### Создаем Сущности и обновляем БД
    composer dump-autoload
    symfony console make:entity User
    symfony console make:migration
